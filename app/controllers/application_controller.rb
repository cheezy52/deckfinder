class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  include ApplicationHelper

  def verify_logged_in
    unless logged_in?
      flash[:errors] = "You must be logged in to perform that action.  Please log in."
      redirect_to new_session_url
    end
  end

  def login!(user)
    user.generate_session_token!
    user.save!
    session[:session_token] = user.session_token
  end

  def logout!
    if current_user
      current_user.generate_session_token!
      current_user.save!
      session[:session_token] = nil
    end
  end
end
