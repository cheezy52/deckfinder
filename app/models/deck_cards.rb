class DeckCard < ActiveRecord::Base
  validates :deck_id, :card_id, :presence => true

  belongs_to :deck
  belongs_to :card
end