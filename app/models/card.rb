# == Schema Information
#
# Table name: cards
#
#  id         :integer          not null, primary key
#  name       :text
#  type       :string(20)
#  set        :integer
#  num_in_box :integer
#  text       :text
#  created_at :datetime
#  updated_at :datetime
#

class Card < ActiveRecord::Base
  TYPES = ["Weapon", "Armor", "Spell", "Item", "Ally", "Blessing",
          "Monster", "Barrier", "Villain", "Henchman",
          "Location", "Scenario", "Adventure", "Adventure Path"]

  validates :name, :presence => true, :uniqueness => true
  validates :type, :presence => true, in: => TYPES

  has_many :deck_cards
  has_many :decks, :through => :deck_cards
  has_many :card_traits, :inverse_of => :card
  has_many :traits, :through => :card_traits
end
