# == Schema Information
#
# Table name: decks
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Deck < ActiveRecord::Base
  validates :user_id, :presence => true

  belongs_to :user

  has_many :deck_cards, :inverse_of => :deck
  has_many :cards, :through => :deck_cards
  has_one :character, :inverse_of => :deck
end
