# == Schema Information
#
# Table name: card_traits
#
#  id         :integer          not null, primary key
#  card_id    :integer
#  trait_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class CardTrait < ActiveRecord::Base
  validates :card_type_id, :trait_id, :presence => true

  belongs_to :card
  belongs_to :trait
end
