# == Schema Information
#
# Table name: traits
#
#  id   :integer          not null, primary key
#  name :string(255)
#

class Trait < ActiveRecord::Base
  validates :name, :presence => true, :uniqueness => true

  has_many :card_traits, :inverse_of => :card
  has_many :cards, :through => :card_traits
end
