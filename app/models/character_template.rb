# == Schema Information
#
# Table name: character_templates
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  title               :string(255)
#  str                 :integer
#  dex                 :integer
#  con                 :integer
#  int                 :integer
#  wis                 :integer
#  cha                 :integer
#  skills              :text
#  powers              :text
#  weapon_slots        :integer
#  armor_slots         :integer
#  spell_slots         :integer
#  item_slots          :integer
#  ally_slots          :integer
#  blessing_slots      :integer
#  possible_tickyboxes :text
#  created_at          :datetime
#  updated_at          :datetime
#

class CharacterTemplate < ActiveRecord::Base
  validates :name, :presence => true
  validates :title, :presence => true, :uniqueness => true

  has_many :characters, :inverse_of => :character_template
end
