# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  username        :string(255)
#  password_digest :string(255)
#  session_token   :string(255)
#  created_at      :datetime
#  updated_at      :datetime
#

class User < ActiveRecord::Base
  validates :username, :presence => true, :uniqueness => true
  validates :password_digest, :presence => true
  validates :password, :length => {:minimum => 6, allow_nil: true}
  before_validation :ensure_session_token

  has_many :decks, inverse_of: :user
  has_many :characters, :through => :decks
  has_many :cards, :through => :decks

  def self.find_by_credentials(params)
    user = self.find_by_username(params[:username])
    return user if user && user.is_password?(params[:password])
    nil
  end

  def generate_session_token!
    self.session_token = SecureRandom::urlsafe_base64(16)
  end

  def ensure_session_token
    self.session_token ||= SecureRandom::urlsafe_base64(16)
  end

  def password=(plaintext)
    @password = plaintext
    self.password_digest = BCrypt::Password.create(plaintext)
  end

  def password
    @password
  end

  def is_password?(plaintext)
    BCrypt::Password.new(password_digest).is_password?(plaintext)
  end
end
