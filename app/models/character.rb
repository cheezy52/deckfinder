# == Schema Information
#
# Table name: characters
#
#  id                    :integer          not null, primary key
#  character_template_id :integer
#  deck_id               :integer
#  tickybox_bitfield     :integer
#  created_at            :datetime
#  updated_at            :datetime
#

class Character < ActiveRecord::Base
  validates :deck, :presence => true, :uniqueness => true
  validates :character_template, :presence => true
  validates :tickybox_bitfield, :presence => true, :numericality => true

  belongs_to :deck, :inverse_of => :character
  belongs_to :character_template, :inverse_of => :characters
  has_one :user, :through => :deck
end
