# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


#Card templates setup
#Boons

#Banes (not relevant for this)

#Non-deck cards

#Character templates setup
kyra_template = CharacterTemplate.create!(:name => "Kyra", :title => "Cleric of Sarenrae",
  :str => "d6", :dex => "d6", :con => "d8", :int => "d4", :wis => "d12", :cha => "d6",
  :skills => "placeholder", :powers => "placeholder",
  :weapon_slots => 2, :armor_slots => 2, :spell_slots => 4, :item_slots => 2,
  :ally_slots => 2, :blessing_slots => 3,
  :possible_tickyboxes => "0: +1 Strength, 1: +1 Weapon Slot")

#User registration/setup
test_user = User.create!({:username => "Ben", :password => "password"})


#User deck creation
test_deck = test_user.deck.create!


#Deck character creation
test_kyra = test_deck.character.create!(:tickybox_bitfield => 0,
  :character_template_id => kyra_template.id)

#Deck cards creation
bsa = Card.create!(:title => "Blessing of Sarenrae",
  :type_id => blessing_type.id, :set => 0, :deck_id => test_deck.id)

