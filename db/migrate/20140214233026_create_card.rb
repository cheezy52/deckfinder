class CreateCard < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.text :name
      t.string :type, :limit => 20
      t.integer :set
      t.integer :num_in_box
      t.text :text

      t.timestamps
    end
    add_index :cards, :name
    add_index :cards, :type
  end
end
