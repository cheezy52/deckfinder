class CreateCharacter < ActiveRecord::Migration
  def change
    create_table :characters do |t|
      t.integer :character_template_id
      t.integer :deck_id
      t.integer :tickybox_bitfield

      t.timestamps
    end
  end
end
