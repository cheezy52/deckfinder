class CreateCardTrait < ActiveRecord::Migration
  def change
    create_table :card_traits do |t|
      t.integer :card_id
      t.integer :trait_id

      t.timestamps
    end
    add_index :card_traits, :card_id
    add_index :card_traits, :trait_id
  end
end
