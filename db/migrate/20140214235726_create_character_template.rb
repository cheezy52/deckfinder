class CreateCharacterTemplate < ActiveRecord::Migration
  def change
    create_table :character_templates do |t|
      t.string :name
      t.string :title
      t.integer :str
      t.integer :dex
      t.integer :con
      t.integer :int
      t.integer :wis
      t.integer :cha
      t.text :skills
      t.text :powers
      t.integer :weapon_slots
      t.integer :armor_slots
      t.integer :spell_slots
      t.integer :item_slots
      t.integer :ally_slots
      t.integer :blessing_slots
      t.text :possible_tickyboxes

      t.timestamps
    end
    add_index :character_templates, :name
    add_index :character_templates, :title
  end
end
