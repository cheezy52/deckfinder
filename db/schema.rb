# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140215010911) do

  create_table "card_traits", force: true do |t|
    t.integer  "card_id"
    t.integer  "trait_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "card_traits", ["card_id"], name: "index_card_traits_on_card_id"
  add_index "card_traits", ["trait_id"], name: "index_card_traits_on_trait_id"

  create_table "cards", force: true do |t|
    t.text     "name"
    t.string   "type",       limit: 20
    t.integer  "set"
    t.integer  "num_in_box"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cards", ["name"], name: "index_cards_on_name"
  add_index "cards", ["type"], name: "index_cards_on_type"

  create_table "character_templates", force: true do |t|
    t.string   "name"
    t.string   "title"
    t.integer  "str"
    t.integer  "dex"
    t.integer  "con"
    t.integer  "int"
    t.integer  "wis"
    t.integer  "cha"
    t.text     "skills"
    t.text     "powers"
    t.integer  "weapon_slots"
    t.integer  "armor_slots"
    t.integer  "spell_slots"
    t.integer  "item_slots"
    t.integer  "ally_slots"
    t.integer  "blessing_slots"
    t.text     "possible_tickyboxes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "character_templates", ["name"], name: "index_character_templates_on_name"
  add_index "character_templates", ["title"], name: "index_character_templates_on_title"

  create_table "characters", force: true do |t|
    t.integer  "character_template_id"
    t.integer  "deck_id"
    t.integer  "tickybox_bitfield"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "deck_cards", force: true do |t|
    t.integer  "deck_id"
    t.integer  "card_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "deck_cards", ["card_id"], name: "index_deck_cards_on_card_id"
  add_index "deck_cards", ["deck_id"], name: "index_deck_cards_on_deck_id"

  create_table "decks", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "decks", ["user_id"], name: "index_decks_on_user_id"

  create_table "traits", force: true do |t|
    t.string "name"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password_digest"
    t.string   "session_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["session_token"], name: "index_users_on_session_token"
  add_index "users", ["username"], name: "index_users_on_username", unique: true

end
