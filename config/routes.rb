Deckfinder::Application.routes.draw do
  root to: "sessions#new"
  resources :users
  resources :cards
  resource :session
  resources :decks
end
